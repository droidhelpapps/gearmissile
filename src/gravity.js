;(function (window, MG) {
    MG.gravity = (function () {
        var dotData = {
            x: window.innerWidth / 2, //horizontal position
            y: window.innerHeight / 2, //vertical position
            v: { //velocity vector
                x: 0,
                y: 0
            },
            cdd: -0.5, //sensor reading scaling
            friction: 0.60 //friction
        };
        
        return {
            getPositionFromMotion: function (event) {
                var motionData = event;
                var x = 0;
                var y = 0;
    
                if (motionData !== null) {
                    x = -motionData.accelerationIncludingGravity.x;
                    y = -motionData.accelerationIncludingGravity.y;
                }
    
                dotData.v.x += (x * -dotData.cdd);
                dotData.v.y += (y * dotData.cdd);
    
                dotData.v.x *= dotData.friction;
                dotData.v.y *= dotData.friction;
                
                var expectedX = dotData.x + dotData.v.x;
                var expectedY = dotData.y + dotData.v.y;
                if ((expectedX < 0) || (expectedY < 0) || (expectedX > window.innerWidth) || (expectedY > window.innerHeight)) {
                    var normalizationX = dotData.x - window.innerWidth;
                    var normalizationY = dotData.y - window.innerHeight;
                    var normalizationLen = Math.sqrt((normalizationX * normalizationX) + (normalizationY * normalizationY));

                    var normalizedX = normalizationX / normalizationLen;
                    var normalizedY = normalizationY / normalizationLen;

                    var velocityPerpendicular = (dotData.v.x * normalizedX) + (dotData.v.y * normalizedY);
                    dotData.v.x = dotData.v.x - 2 * velocityPerpendicular * normalizedX;
                    dotData.v.y = dotData.v.y - 2 * velocityPerpendicular * normalizedY;
                } else {
                    dotData.x += dotData.v.x;
                    dotData.y += dotData.v.y;
                }

                return {
                    x: dotData.x,
                    y: dotData.y
                };
            }
        };
    })();
})(window, window.MG);