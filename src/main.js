;(function (window, tizen, MG) {
	MG.init = function () {
	    MG.fog.init();
	    MG.banner.init();
	    MG.game.init();
	    MG.hud.init();

	    document.addEventListener('mousemove', function(evt){
			if (MG.game.isGameRunning()) {
				MG.game.onMouseMove(evt.clientX, evt.clientY);
			}
         }, false);

	    window.addEventListener('devicemotion', function(evt){
			if (MG.game.isGameRunning()) {
				var position = MG.gravity.getPositionFromMotion(evt);
				MG.game.onMouseMove(position.x, position.y);
			}
	    }, false);
	    
	    window.addEventListener('mousedown', MG.game.onMouseClick, false);

		// Add hardware event listener
		document.addEventListener("tizenhwkey", function keyEventHandler(event) {
	        if (event.keyName === "back") {
	            try {
					// If the back key is pressed, exit the application.
					tizen.power.release('CPU');
					tizen.power.release('SCREEN');
	                tizen.application.getCurrentApplication().exit();
	            } catch (ignore) {}
	        }
	    });
		
	    var update = function (dt) {
	        MG.fog.update(dt);
	        MG.game.update(dt);
	        MG.hud.update(dt);
	        MG.banner.update(dt);


	        MG.fog.updateDOM();
	        MG.game.updateDOM();
	        MG.hud.updateDOM();
	        MG.banner.updateDOM();
	    };

	    var lastTick = 0;
	    var zeroCounter = 0;
	    var useFallback = false;

	    if (!window.requestAnimationFrame) {
	        useFallback = true;
		}
		
		if (tizen && tizen.power) {
			tizen.power.request('CPU', 'CPU_AWAKE');
			tizen.power.request('SCREEN', 'SCREEN_NORMAL');
		}

	    var mainLoop = function(thisTick) {
	        var dt;

	        // Some browsers don't pass in a time.  If `thisTick` isn't set for
	        //  more than a few frames fall back to `setTimeout`
	        if (!thisTick) {
	            zeroCounter += 1;
	        } else {
	            zeroCounter = 0;
	        }
	        if (zeroCounter > 10) {
	            useFallback = true;
	        }

	        thisTick = thisTick || 0;
	        if (useFallback) {
	            dt = 1/30;
	        } else {
	            dt = (thisTick - lastTick)/1000;
	        }
	        // pretend that the frame rate is actually higher if it drops below
	        // 10fps in order to avoid wierdness
	        if (dt > 1/10) {
	            dt = 1/10;
	        }

	        lastTick = thisTick;

	        update(dt);

	        if (useFallback) {
	            window.setTimeout(mainLoop, 1000 / 30);
	        } else {
	            window.requestAnimationFrame(mainLoop);
	        }
	    };

	    mainLoop();
	};

	window.onload = MG.init;
})(window, window.tizen,  window.MG);